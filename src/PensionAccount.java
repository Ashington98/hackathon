// PensionAccount.java
// Ashley Oldershaw October 2017
// A pension account, you can't take money out until you retire, this is mainly for score

import java.math.*;

public class PensionAccount extends BankAccount {

  public PensionAccount (Player newPlayer, double initialBalance, double initialRate) {
    super(newPlayer, initialBalance);
    rate = initialRate;
  }

  // we don't want to be able to take out money
  @Override
  public boolean withdraw(BankAccount recipient, double withdrawValue) {return false;}
}

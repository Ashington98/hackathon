// Mortgage.java
// Ashley Oldershaw October 2017
// The basic bank account

public class Mortgage extends BankAccount {

  public Mortgage (Player player, double initialBalance, double initialRate) {
    super (player, initialBalance);
    rate = initialRate;
    quarterlyPayment = -(initialBalance / 80.0);
  }

  // we don't want to be able to take out money
  @Override
  public boolean withdraw(BankAccount recipient, double withdrawValue) { return false; }
}

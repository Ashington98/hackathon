// CashISA.java
// Ashley Oldershaw October 2017
// The savings account, it's used for medium term saving

public class CashISA extends BankAccount {

  protected double allowance;

  public CashISA (Player newPlayer, double initialBalance, double initialRate, double initialAllowance) {
    super (newPlayer, initialBalance);
    rate = initialRate;
    allowance = initialAllowance;
  }

  public void addAllowance(double newAllowance) {
    allowance += newAllowance;
  }
}
